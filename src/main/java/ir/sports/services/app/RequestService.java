package ir.sports.services.app;

import ir.sports.entities.User;
import ir.sports.entities.app.*;
import ir.sports.repositories.app.RelationCacheRepository;
import ir.sports.repositories.app.RelationRepository;
import ir.sports.repositories.app.RequestRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RequestService extends BaseService {
    @Autowired
    private RequestRepository requestRepository;
    @Autowired
    private RelationRepository relationRepository;
    @Autowired
    private RelationCacheRepository relationCacheRepository;

    public void sendRequest(User followee) {
        Request request = new Request();
        request.setFollowee(followee);
        request.setFollower(getLoggedInUser());
        try {
            requestRepository.saveAndFlush(request);
        } catch (Exception e) {
            if (e.getCause().getCause().toString().contains("Detail: Key (follower_id, follow_id)=("))
                throw new RuntimeException(getErrorMessage("requestFound"));
            throw e;
        }
    }

    public List<FollowerOnly> findAllReceivedRequest() {
        return requestRepository.findAllByFollowee(getLoggedInUser());
    }

    public List<FolloweeOnly> findAllSentRequest() {
        return requestRepository.findAllByFollower(getLoggedInUser());
    }

    public void unsentRequest(User followee) {
        requestRepository.deleteAllByFolloweeAndFollower(followee, getLoggedInUser());
    }

    public void rejectRequest(User follower) {
        requestRepository.deleteAllByFolloweeAndFollower(getLoggedInUser(), follower);
    }

    public void acceptRequest(User follower) {
        Optional<RelationCache> user = relationCacheRepository.findByUsername(getLoggedInUsername());
        Optional<RelationCache> followCache = relationCacheRepository.findByUserId(follower.getId());
        User loggedInUser;
        if (followCache.isPresent()) {
            followCache.get().setFollowingsUpdated(false);
            relationCacheRepository.save(followCache.get());
        }
        if (user.isPresent()) {
            user.get().setFollowersUpdated(false);
            relationCacheRepository.save(user.get());
            loggedInUser = new User().build(user.get().getUserId());
        } else {
            loggedInUser = getLoggedInUser();
        }
        if (!requestRepository.findAllByFolloweeAndFollower(loggedInUser, follower).isPresent())
            throw new RuntimeException(getErrorMessage("requestNotFound"));
        Relation relation = new Relation();
        relation.setFollower(follower);
        relation.setFollowee(loggedInUser);
        try {
            relationRepository.saveAndFlush(relation);
        } catch (Exception e) {
            if (e.getCause().getCause().toString().contains("Detail: Key (follower_id, follow_id)=("))
                throw new RuntimeException(getErrorMessage("requestFound"));
            throw e;
        }
        requestRepository.deleteAllByFolloweeAndFollower(loggedInUser, follower);
    }
}
