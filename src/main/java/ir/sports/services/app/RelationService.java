package ir.sports.services.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import ir.sports.entities.User;
import ir.sports.entities.app.RelationCache;
import ir.sports.repositories.app.RelationCacheRepository;
import ir.sports.repositories.app.RelationRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RelationService extends BaseService {
    @Autowired
    private RelationRepository relationRepository;
    @Autowired
    private RelationCacheRepository relationCacheRepository;
    private static ObjectWriter ow = new ObjectMapper().writer();

    public String findAllFollowers() throws JsonProcessingException {
        Optional<RelationCache> cache = relationCacheRepository.findByUsername(getLoggedInUsername());
        if (cache.isPresent()) {
            if (cache.get().isFollowersUpdated()) {
                return cache.get().getFollowersList();
            } else {
                String result = ow.writeValueAsString(relationRepository.findAllByFollowee(new User().build(cache.get().getUserId())));
                cache.get().setFollowersUpdated(true);
                cache.get().setFollowersList(result);
                relationCacheRepository.save(cache.get());
                return result;
            }
        }
        User loggedInUser = getLoggedInUser();
        String result = ow.writeValueAsString(relationRepository.findAllByFollowee(loggedInUser));
        RelationCache relationCache = new RelationCache();
        relationCache.setFollowersList(result);
        relationCache.setUserId(loggedInUser.getId());
        relationCache.setUsername(getLoggedInUsername());
        relationCache.setFollowersUpdated(true);
        relationCache.setFollowingsUpdated(false);
        relationCacheRepository.save(relationCache);
        return result;
    }

    public String findAllFollowings() throws JsonProcessingException {
        Optional<RelationCache> cache = relationCacheRepository.findByUsername(getLoggedInUsername());
        if (cache.isPresent()) {
            if (cache.get().isFollowingsUpdated()) {
                return cache.get().getFollowingsList();
            } else {
                String result = ow.writeValueAsString(relationRepository.findAllByFollower(new User().build(cache.get().getUserId())));
                cache.get().setFollowingsUpdated(true);
                cache.get().setFollowingsList(result);
                relationCacheRepository.save(cache.get());
                return result;
            }
        }
        User loggedInUser = getLoggedInUser();
        String result = ow.writeValueAsString(relationRepository.findAllByFollower(loggedInUser));
        RelationCache relationCache = new RelationCache();
        relationCache.setFollowingsList(result);
        relationCache.setUserId(loggedInUser.getId());
        relationCache.setUsername(getLoggedInUsername());
        relationCache.setFollowersUpdated(false);
        relationCache.setFollowingsUpdated(true);
        relationCacheRepository.save(relationCache);
        return result;
    }

    public void unfollow(User followee) {
        Optional<RelationCache> user = relationCacheRepository.findByUsername(getLoggedInUsername());
        Optional<RelationCache> followCache = relationCacheRepository.findByUserId(followee.getId());
        if (followCache.isPresent()) {
            followCache.get().setFollowersUpdated(false);
            relationCacheRepository.save(followCache.get());
        }
        if (user.isPresent()) {
            user.get().setFollowingsUpdated(false);
            relationCacheRepository.save(user.get());
            relationRepository.deleteAllByFolloweeAndFollower(followee, new User().build(user.get().getUserId()));
        } else {
            relationRepository.deleteAllByFolloweeAndFollower(followee, getLoggedInUser());
        }
    }

    public void removeFollower(User follower) {
        Optional<RelationCache> user = relationCacheRepository.findByUsername(getLoggedInUsername());
        Optional<RelationCache> followCache = relationCacheRepository.findByUserId(follower.getId());
        if (followCache.isPresent()) {
            followCache.get().setFollowingsUpdated(false);
            relationCacheRepository.save(followCache.get());
        }
        if (user.isPresent()) {
            user.get().setFollowersUpdated(false);
            relationCacheRepository.save(user.get());
            relationRepository.deleteAllByFolloweeAndFollower(new User().build(user.get().getUserId()), follower);
        } else {
            relationRepository.deleteAllByFolloweeAndFollower(getLoggedInUser(), follower);
        }
    }
}
