package ir.sports.services.context;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.Event;
import ir.sports.repositories.context.EventRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EventService extends BaseService {
    @Autowired
    private EventRepository eventRepository;

    public List<Event> findAll(PageableDTO dto) {
        return eventRepository.findAll(getPageable(dto)).getContent();
    }

    public Event findById(Long id) {
        Optional<Event> event = eventRepository.findById(id);
        if (event.isPresent())
            return event.get();
        throw new RuntimeException(getErrorMessage("contentNotFound"));
    }

    public Event save(Event dto) {
        return eventRepository.save(dto);
    }

    public void deleteById(Long id) {
        try {
            eventRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }
}
