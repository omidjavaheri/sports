package ir.sports.services.context.board;

import ir.sports.dto.v1.context.AthleticBoardDTO;
import ir.sports.entities.context.board.News;
import ir.sports.repositories.context.board.NewsRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class NewsService extends BaseService {
    @Autowired
    private NewsRepository newsRepository;

    public List<News> findAllByAthleticBoard(AthleticBoardDTO dto) {
        return newsRepository.findAllByAthleticBoard(dto.getAthleticBoard(), getPageable(dto.getPageableDTO()));
    }

    public News save(News dto){
        return newsRepository.save(dto);
    }

    public void deleteById(Long id) {
        try {
            newsRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }
}
