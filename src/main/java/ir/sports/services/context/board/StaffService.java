package ir.sports.services.context.board;

import ir.sports.dto.v1.context.AthleticBoardDTO;
import ir.sports.entities.context.board.Staff;
import ir.sports.repositories.context.board.StaffRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StaffService extends BaseService {
    @Autowired
    private StaffRepository staffRepository;

    public List<Staff> findAllByAthleticBoard(AthleticBoardDTO dto) {
        return staffRepository.findAllByAthleticBoard(dto.getAthleticBoard(), getPageable(dto.getPageableDTO()));
    }

    public Staff save(Staff dto) {
        return staffRepository.save(dto);
    }

    public void deleteById(Long id) {
        try {
            staffRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }
}
