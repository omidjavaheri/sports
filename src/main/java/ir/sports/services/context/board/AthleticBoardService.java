package ir.sports.services.context.board;

import ir.sports.dto.v1.context.*;
import ir.sports.entities.context.board.AthleticBoard;
import ir.sports.entities.context.board.AthleticBoardOnly;
import ir.sports.entities.context.SportOnly;
import ir.sports.repositories.context.board.AthleticBoardRepository;
import ir.sports.repositories.context.board.AthleticBoardSportRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AthleticBoardService extends BaseService {
    @Autowired
    private AthleticBoardRepository athleticBoardRepository;
    @Autowired
    private AthleticBoardSportRepository athleticBoardSportRepository;

    public Iterable<AthleticBoard> findAll() {
        return athleticBoardRepository.findAll();
    }

    public void deleteById(Long id) {
        try {
            athleticBoardRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }

    public AthleticBoard save(AthleticBoard athleticBoard) {
        return athleticBoardRepository.save(athleticBoard);
    }

    public List<AthleticBoard> findAllByName(NameDTO dto) {
        return athleticBoardRepository.findAllByName(dto.getName(), getPageable(dto.getPageableDTO()));
    }

    public List<AthleticBoard> findAllByProvince(ProvinceDTO dto) {
        return athleticBoardRepository.findAllByProvince(dto.getProvince(), getPageable(dto.getPageableDTO()));
    }

    public List<AthleticBoard> findAllByCity(CityDTO dto) {
        return athleticBoardRepository.findAllByCity(dto.getCity(), getPageable(dto.getPageableDTO()));
    }

    public List<AthleticBoardOnly> findAllBySport(SportDTO dto) {
        return athleticBoardSportRepository.findAllBySport(dto.getSport(), getPageable(dto.getPageableDTO()));
    }

    public List<SportOnly> findAllSports(AthleticBoardDTO dto) {
        return athleticBoardSportRepository.findByAthleticBoard(dto.getAthleticBoard(), getPageable(dto.getPageableDTO()));
    }

    public AthleticBoard findById (Long id){
        Optional<AthleticBoard> athleticBoard = athleticBoardRepository.findById(id);
        if(athleticBoard.isPresent())
            return athleticBoard.get();
        throw new RuntimeException(getErrorMessage("contentNotFound"));
    }
}
