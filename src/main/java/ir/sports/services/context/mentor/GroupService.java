package ir.sports.services.context.mentor;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.MentorDTO;
import ir.sports.entities.context.mentor.Group;
import ir.sports.repositories.context.mentor.GroupRepository;
import ir.sports.repositories.context.mentor.GroupUserRepository;
import ir.sports.repositories.context.mentor.MentorRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GroupService extends BaseService {
    @Autowired
    private GroupRepository groupRepository;
    @Autowired
    private MentorRepository mentorRepository;
    @Autowired
    private GroupUserRepository groupUserRepository;

    public List<Group> findAll(PageableDTO dto) {
        return groupRepository.findByMentor(mentorRepository.findByUsername(getLoggedInUsername()), getPageable(dto));
    }

    public List<Group> findAllByMentor(MentorDTO dto) {
        return groupRepository.findByMentor(dto.getMentor(), getPageable(dto.getPageableDTO()));
    }

    public Group save(Group dto) {
        if (dto.getMentor() == null) {
            dto.setMentor(mentorRepository.findByUsername(getLoggedInUsername()));
            return groupRepository.save(dto);
        } else if (mentorRepository.findByUsername(getLoggedInUsername()).getId().equals(dto.getMentor().getId()))
            return groupRepository.save(dto);
        throw new RuntimeException(getErrorMessage("mentorNotAssigned"));
    }

    public void deleteById(Long id) {
        Optional<Group> group = groupRepository.findById(id);
        if (!group.isPresent())
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        if (!mentorRepository.findByUsername(getLoggedInUsername()).getId().equals(group.get().getMentor().getId()))
            throw new RuntimeException(getErrorMessage("mentorNotAssigned"));
        groupUserRepository.deleteAllByGroup(group.get());
        groupRepository.deleteById(id);
    }

    public List<Group> adminFindAll(PageableDTO dto) {
        return groupRepository.findAll(getPageable(dto)).getContent();
    }

    public Group adminSave(Group dto) {
        return groupRepository.save(dto);
    }

    public void adminDeleteById(Long id) {
        try {
            groupUserRepository.deleteAllByGroupId(id);
            groupRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }
}
