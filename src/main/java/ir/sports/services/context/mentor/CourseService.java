package ir.sports.services.context.mentor;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.MentorDTO;
import ir.sports.entities.context.mentor.Course;
import ir.sports.repositories.context.mentor.CourseRepository;
import ir.sports.repositories.context.mentor.MentorRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CourseService extends BaseService {
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private MentorRepository mentorRepository;

    public List<Course> findAll(PageableDTO dto) {
        return courseRepository.findByMentor(mentorRepository.findByUsername(getLoggedInUsername()), getPageable(dto));
    }

    public List<Course> findAllByMentor(MentorDTO dto) {
        return courseRepository.findByMentor(dto.getMentor(), getPageable(dto.getPageableDTO()));
    }

    public Course save(Course dto) {
        if (dto.getMentor() == null) {
            dto.setMentor(mentorRepository.findByUsername(getLoggedInUsername()));
            return courseRepository.save(dto);
        } else if (mentorRepository.findByUsername(getLoggedInUsername()).getId().equals(dto.getMentor().getId()))
            return courseRepository.save(dto);
        throw new RuntimeException(getErrorMessage("mentorNotAssigned"));
    }

    public void deleteById(Long id) {
        Optional<Course> course = courseRepository.findById(id);
        if (!course.isPresent())
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        if (!mentorRepository.findByUsername(getLoggedInUsername()).getId().equals(course.get().getMentor().getId()))
            throw new RuntimeException(getErrorMessage("mentorNotAssigned"));
        courseRepository.deleteById(id);
    }

    public List<Course> adminFindAll(PageableDTO dto) {
        return courseRepository.findAll(getPageable(dto)).getContent();
    }

    public Course adminSave(Course dto) {
        return courseRepository.save(dto);
    }

    public void adminDeleteById(Long id) {
        try {
            courseRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }
}
