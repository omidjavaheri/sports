package ir.sports.services.context.mentor;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.GroupDTO;
import ir.sports.entities.context.Event;
import ir.sports.entities.context.mentor.Group;
import ir.sports.entities.context.mentor.GroupOnly;
import ir.sports.entities.context.mentor.GroupUser;
import ir.sports.repositories.context.EventRepository;
import ir.sports.repositories.context.mentor.GroupUserRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GroupSubscribeService extends BaseService {
    @Autowired
    private GroupUserRepository groupUserRepository;
    @Autowired
    private EventRepository eventRepository;

    public List<GroupOnly> findAll(PageableDTO dto) {
        return groupUserRepository.findAllByUsername(getLoggedInUsername(), getPageable(dto));
    }

    public void follow(Group group) {
        GroupUser groupUser = new GroupUser();
        groupUser.setUsername(getLoggedInUsername());
        groupUser.setGroup(group);
        groupUserRepository.save(groupUser);
    }

    public void unfollow(Group group) {
        groupUserRepository.deleteAllByGroupAndUsername(group, getLoggedInUsername());
    }

    public List<Event> findAllGroupEvents(GroupDTO dto) {
        return eventRepository.findAllByGroup(dto.getGroup(), getPageable(dto.getPageableDTO()));
    }
}
