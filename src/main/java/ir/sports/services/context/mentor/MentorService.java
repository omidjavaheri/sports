package ir.sports.services.context.mentor;

import ir.sports.dto.v1.context.*;
import ir.sports.entities.Role;
import ir.sports.entities.User;
import ir.sports.entities.context.*;
import ir.sports.entities.context.mentor.Mentor;
import ir.sports.entities.context.mentor.MentorOnly;
import ir.sports.entities.context.mentor.MentorSport;
import ir.sports.repositories.UserRepository;
import ir.sports.repositories.context.mentor.MentorRepository;
import ir.sports.repositories.context.mentor.MentorSportRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MentorService extends BaseService {
    @Autowired
    private MentorRepository mentorRepository;
    @Autowired
    private MentorSportRepository mentorSportRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public Iterable<Mentor> findAll() {
        return mentorRepository.findAll();
    }

    public void deleteById(Long id) {
        try {
            mentorRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }

    public User simpleSave(Mentor dto) {
        User user = dto.getUser();
        user.setPlain(user.getPassword());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.MENXXTOR.getServer());
        User registeredUser = null;
        try {
            registeredUser = userRepository.saveAndFlush(user);
        } catch (Exception e) {
            if (e.getCause().getCause().toString().contains("Detail: Key (mobile)=("))
                throw new RuntimeException(getErrorMessage("foundMobile"));
            else if (e.getCause().getCause().toString().contains("Detail: Key (username)=("))
                throw new RuntimeException(getErrorMessage("foundUsername"));
            throw e;
        }
        dto.setUsername(dto.getUser().getUsername());
        mentorRepository.save(dto);
        return registeredUser;
    }

    public User save(Mentor dto) {
        User registeredUser = null;
        if (dto.getId() == null)
            registeredUser = simpleSave(dto);
        else
            mentorSportRepository.deleteAllByMentor(dto);
        for (Long id : dto.getSportsId()) {
            MentorSport mentorSport = new MentorSport();
            Sport sport = new Sport();
            sport.setId(id);
            mentorSport.setMentor(dto);
            mentorSport.setSport(sport);
            mentorSportRepository.save(mentorSport);
        }
        if (dto.getId() != null)
            mentorRepository.save(dto);
        return registeredUser;
    }

    public List<Mentor> findAllByName(NameDTO dto) {
        return mentorRepository.findAllByName(dto.getName(), getPageable(dto.getPageableDTO()));
    }

    public List<Mentor> findAllByProvince(ProvinceDTO dto) {
        return mentorRepository.findAllByProvince(dto.getProvince(), getPageable(dto.getPageableDTO()));
    }

    public List<Mentor> findAllByCity(CityDTO dto) {
        return mentorRepository.findAllByCity(dto.getCity(), getPageable(dto.getPageableDTO()));
    }

    public List<SportOnly> findAllSports(MentorDTO dto) {
        return mentorSportRepository.findAllByMentor(dto.getMentor(), getPageable(dto.getPageableDTO()));
    }

    public List<MentorOnly> findAllBySport(SportDTO dto) {
        return mentorSportRepository.findAllBySport(dto.getSport(), getPageable(dto.getPageableDTO()));
    }

    public Mentor findById(Long id) {
        Optional<Mentor> mentor = mentorRepository.findById(id);
        if (mentor.isPresent()) {
            Mentor m = mentor.get();
            m.setSportsId(mentorSportRepository.findSportsIdsByMentor(m));
            return m;
        }
        throw new RuntimeException(getErrorMessage("contentNotFound"));
    }
}
