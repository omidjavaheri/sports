package ir.sports.services.context.gym;

import ir.sports.dto.v1.context.*;
import ir.sports.entities.context.*;
import ir.sports.entities.context.gym.Gym;
import ir.sports.entities.context.gym.GymOnly;
import ir.sports.entities.context.gym.GymSport;
import ir.sports.repositories.context.gym.GymRepository;
import ir.sports.repositories.context.gym.GymSportRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GymService extends BaseService {
    @Autowired
    private GymRepository gymRepository;
    @Autowired
    private GymSportRepository gymSportRepository;

    public Iterable<Gym> findAll() {
        return gymRepository.findAll();
    }

    public void deleteById(Long id) {
        try {
            gymRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }

    public Gym simpleSave(Gym gym){
        return gymRepository.save(gym);
    }

    public Gym save(Gym gym) {
        if(gym.getId()==null)
            simpleSave(gym);
        else
            gymSportRepository.deleteAllByGym(gym);
        for (Long id:gym.getSportsId()) {
            GymSport gymSport = new GymSport();
            Sport sport = new Sport();
            sport.setId(id);
            gymSport.setGym(gym);
            gymSport.setSport(sport);
            gymSportRepository.save(gymSport);
        }
        if(gym.getId()==null)
            return null;
        return gymRepository.save(gym);
    }

    public List<Gym> findAllByName(NameDTO dto) {
        return gymRepository.findAllByName(dto.getName(), getPageable(dto.getPageableDTO()));
    }

    public List<Gym> findAllByProvince(ProvinceDTO dto) {
        return gymRepository.findAllByProvince(dto.getProvince(), getPageable(dto.getPageableDTO()));
    }

    public List<Gym> findAllByCity(CityDTO dto) {
        return gymRepository.findAllByCity(dto.getCity(), getPageable(dto.getPageableDTO()));
    }

    public List<SportOnly> findAllSports(GymDTO dto){
        return gymSportRepository.findAllByGym(dto.getGym(), getPageable(dto.getPageableDTO()));
    }

    public List<GymOnly> findAllGymBySport(SportDTO dto) {
        return gymSportRepository.findAllBySport(dto.getSport(), getPageable(dto.getPageableDTO()));
    }

    public Gym findById (Long id){
        Optional<Gym> gym = gymRepository.findById(id);
        if(gym.isPresent()) {
            Gym g = gym.get();
            g.setSportsId(gymSportRepository.findSportsIdsByGym(g));
            return g;
        }
        throw new RuntimeException(getErrorMessage("contentNotFound"));
    }
}
