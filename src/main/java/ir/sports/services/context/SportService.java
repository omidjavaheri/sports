package ir.sports.services.context;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.NameDTO;
import ir.sports.entities.context.Sport;
import ir.sports.entities.context.board.AthleticBoard;
import ir.sports.entities.context.board.AthleticBoardSport;
import ir.sports.repositories.context.SportRepository;
import ir.sports.repositories.context.board.AthleticBoardSportRepository;
import ir.sports.services.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SportService extends BaseService {
    @Autowired
    private SportRepository sportRepository;
    @Autowired
    private AthleticBoardSportRepository athleticBoardSportRepository;

    public List<Sport> findAll(PageableDTO dto) {
        return sportRepository.findAll(getPageable(dto)).getContent();
    }

    public List<Sport> findAllByNameContaining(NameDTO dto){
        return sportRepository.findAllByNameContaining(dto.getName(), getPageable(dto.getPageableDTO()));
    }

    public Sport findById (Long id){
        Optional<Sport> sport = sportRepository.findById(id);
        if(sport.isPresent()) {
            Sport s = sport.get();
            s.setAthleticBoardsId(athleticBoardSportRepository.findAthleticBoardIdsBySport(s));
            return s;
        }
        throw new RuntimeException(getErrorMessage("contentNotFound"));
    }

    public void deleteById(Long id) {
        try {
            sportRepository.deleteById(id);
        } catch (Exception e) {
            throw new RuntimeException(getErrorMessage("contentNotFound"));
        }
    }

    public Sport simpleSave(Sport sport){
        return sportRepository.save(sport);
    }

    public Sport save(Sport sport) {
        if(sport.getId()==null)
            simpleSave(sport);
        else
            athleticBoardSportRepository.deleteAllBySport(sport);
        for (Long id : sport.getAthleticBoardsId()) {
            AthleticBoardSport athleticBoardSport = new AthleticBoardSport();
            AthleticBoard athleticBoard = new AthleticBoard();
            athleticBoard.setId(id);
            athleticBoardSport.setAthleticBoard(athleticBoard);
            athleticBoardSport.setSport(sport);
            athleticBoardSportRepository.save(athleticBoardSport);
        }
        if(sport.getId()==null)
            return null;
        return sportRepository.save(sport);
    }
}