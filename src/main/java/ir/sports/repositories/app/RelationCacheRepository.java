package ir.sports.repositories.app;

import ir.sports.entities.app.RelationCache;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RelationCacheRepository extends JpaRepository<RelationCache, Long> {
    Optional<RelationCache> findByUsername(String username);
    Optional<RelationCache> findByUserId(Long userId);
}
