package ir.sports.repositories.app;

import ir.sports.entities.User;
import ir.sports.entities.app.FolloweeOnly;
import ir.sports.entities.app.FollowerOnly;
import ir.sports.entities.app.Request;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RequestRepository extends JpaRepository<Request, Long> {
    List<FolloweeOnly> findAllByFollower(User follower);
    Optional<Request> findAllByFolloweeAndFollower(User followee, User follower);
    List<FollowerOnly> findAllByFollowee(User followee);
    void deleteAllByFolloweeAndFollower(User followee, User follower);
}
