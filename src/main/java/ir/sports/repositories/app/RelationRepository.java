package ir.sports.repositories.app;

import ir.sports.entities.User;
import ir.sports.entities.app.FolloweeOnly;
import ir.sports.entities.app.FollowerOnly;
import ir.sports.entities.app.Relation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RelationRepository extends JpaRepository<Relation, Long> {
    List<FolloweeOnly> findAllByFollower(User follower);
    List<FollowerOnly> findAllByFollowee(User followee);
    void deleteAllByFolloweeAndFollower(User followee, User follower);
}
