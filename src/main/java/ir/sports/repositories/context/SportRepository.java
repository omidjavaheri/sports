package ir.sports.repositories.context;

import ir.sports.entities.context.Sport;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface SportRepository extends PagingAndSortingRepository<Sport, Long> {
    Page<Sport> findAll(Pageable pageable);

    List<Sport> findAllByNameContaining(String name, Pageable pageable);
}
