package ir.sports.repositories.context.mentor;

import ir.sports.entities.context.mentor.Mentor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MentorRepository extends PagingAndSortingRepository<Mentor, Long> {
    List<Mentor> findAllByName(String name, Pageable pageable);

    List<Mentor> findAllByProvince(String province, Pageable pageable);

    List<Mentor> findAllByCity(String city, Pageable pageable);

    Mentor findByUsername(String username);
}
