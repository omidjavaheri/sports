package ir.sports.repositories.context.mentor;

import ir.sports.entities.context.mentor.Course;
import ir.sports.entities.context.mentor.Mentor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CourseRepository extends PagingAndSortingRepository<Course, Long> {
    List<Course> findByMentor(Mentor mentor, Pageable pageable);

    Page<Course> findAll(Pageable pageable);
}
