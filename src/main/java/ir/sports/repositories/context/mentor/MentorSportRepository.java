package ir.sports.repositories.context.mentor;

import ir.sports.entities.context.*;
import ir.sports.entities.context.mentor.Mentor;
import ir.sports.entities.context.mentor.MentorOnly;
import ir.sports.entities.context.mentor.MentorSport;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MentorSportRepository extends PagingAndSortingRepository<MentorSport, Long> {
    List<MentorOnly> findAllBySport(Sport sport, Pageable pageable);

    List<SportOnly> findAllByMentor(Mentor mentor, Pageable pageable);

    void deleteAllByMentor(Mentor mentor);

    @Query(value = "select ms.sport.id from MentorSport ms where ms.mentor = :m")
    List<Long> findSportsIdsByMentor(@Param("m") Mentor m);
}
