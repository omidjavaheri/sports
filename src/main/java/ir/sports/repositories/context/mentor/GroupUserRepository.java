package ir.sports.repositories.context.mentor;

import ir.sports.entities.context.mentor.Group;
import ir.sports.entities.context.mentor.GroupOnly;
import ir.sports.entities.context.mentor.GroupUser;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface GroupUserRepository extends PagingAndSortingRepository<GroupUser, Long> {
    List<GroupOnly> findAllByUsername(String username, Pageable pageable);

    void deleteAllByGroup(Group group);

    void deleteAllByGroupId(Long id);

    void deleteAllByGroupAndUsername(Group group, String username);
}
