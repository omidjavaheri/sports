package ir.sports.repositories.context.mentor;

import ir.sports.entities.context.mentor.Group;
import ir.sports.entities.context.mentor.Mentor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface GroupRepository extends PagingAndSortingRepository<Group, Long> {
    List<Group> findByMentor(Mentor mentor, Pageable pageable);

    Page<Group> findAll(Pageable pageable);
}
