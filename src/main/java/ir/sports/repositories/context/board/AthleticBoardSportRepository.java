package ir.sports.repositories.context.board;

import ir.sports.entities.context.*;
import ir.sports.entities.context.board.AthleticBoard;
import ir.sports.entities.context.board.AthleticBoardOnly;
import ir.sports.entities.context.board.AthleticBoardSport;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AthleticBoardSportRepository extends PagingAndSortingRepository<AthleticBoardSport, Long> {
    List<SportOnly> findByAthleticBoard(AthleticBoard athleticBoard, Pageable pageable);

    List<AthleticBoardOnly> findAllBySport(Sport sport, Pageable pageable);

    void deleteAllBySport(Sport sport);

    @Query(value = "select ab.athleticBoard.id from AthleticBoardSport ab where ab.sport = :s")
    List<Long> findAthleticBoardIdsBySport(@Param("s") Sport s);

}
