package ir.sports.repositories.context.board;

import ir.sports.entities.context.board.AthleticBoard;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AthleticBoardRepository extends PagingAndSortingRepository<AthleticBoard, Long> {
    List<AthleticBoard> findAllByName(String name, Pageable pageable);

    List<AthleticBoard> findAllByProvince(String province, Pageable pageable);

    List<AthleticBoard> findAllByCity(String city, Pageable pageable);
}
