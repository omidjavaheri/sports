package ir.sports.repositories.context.board;

import ir.sports.entities.context.board.AthleticBoard;
import ir.sports.entities.context.board.News;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface NewsRepository extends PagingAndSortingRepository<News, Long> {
    List<News> findAllByAthleticBoard(AthleticBoard athleticBoard, Pageable pageable);
}
