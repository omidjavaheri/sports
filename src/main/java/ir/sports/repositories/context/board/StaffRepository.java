package ir.sports.repositories.context.board;

import ir.sports.entities.context.board.AthleticBoard;
import ir.sports.entities.context.board.Staff;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StaffRepository extends PagingAndSortingRepository<Staff, Long> {
    List<Staff> findAllByAthleticBoard(AthleticBoard athleticBoard, Pageable pageable);
}
