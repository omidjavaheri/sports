package ir.sports.repositories.context;

import ir.sports.entities.context.Event;
import ir.sports.entities.context.mentor.Group;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface EventRepository extends PagingAndSortingRepository<Event, Long> {
    Page<Event> findAll(Pageable pageable);

    List<Event> findAllByGroup(Group group, Pageable pageable);
}
