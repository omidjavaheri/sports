package ir.sports.repositories.context.gym;

import ir.sports.entities.context.*;
import ir.sports.entities.context.gym.Gym;
import ir.sports.entities.context.gym.GymOnly;
import ir.sports.entities.context.gym.GymSport;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GymSportRepository extends PagingAndSortingRepository<GymSport, Long> {
    List<SportOnly> findAllByGym(Gym gym, Pageable pageable);

    List<GymOnly> findAllBySport(Sport sport, Pageable pageable);

    void deleteAllByGym(Gym gym);

    @Query(value = "select gs.sport.id from GymSport gs where gs.gym = :g")
    List<Long> findSportsIdsByGym(@Param("g") Gym g);
}
