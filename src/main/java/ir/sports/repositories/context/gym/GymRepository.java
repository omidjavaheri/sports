package ir.sports.repositories.context.gym;

import ir.sports.entities.context.gym.Gym;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface GymRepository extends PagingAndSortingRepository<Gym, Long> {
    List<Gym> findAllByName(String name, Pageable pageable);

    List<Gym> findAllByProvince(String province, Pageable pageable);

    List<Gym> findAllByCity(String city, Pageable pageable);

}
