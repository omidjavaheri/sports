package ir.sports.entities;

public interface UsernameOnly {
    String getUsername();
}
