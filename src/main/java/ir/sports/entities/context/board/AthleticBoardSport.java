package ir.sports.entities.context.board;

import ir.sports.entities.context.Sport;

import javax.persistence.*;

@Entity
@Table(name = "athletic_board_sport")
public class AthleticBoardSport {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private AthleticBoard athleticBoard;
    @ManyToOne
    private Sport sport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AthleticBoard getAthleticBoard() {
        return athleticBoard;
    }

    public void setAthleticBoard(AthleticBoard athleticBoard) {
        this.athleticBoard = athleticBoard;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }
}
