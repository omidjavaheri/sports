package ir.sports.entities.context.board;

import javax.persistence.*;

@Entity
@Table(name = "news")
public class News {
    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String content;
    @ManyToOne
    private AthleticBoard athleticBoard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public AthleticBoard getAthleticBoard() {
        return athleticBoard;
    }

    public void setAthleticBoard(AthleticBoard athleticBoard) {
        this.athleticBoard = athleticBoard;
    }
}
