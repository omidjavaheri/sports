package ir.sports.entities.context.board;

public interface AthleticBoardOnly {
    AthleticBoard getAthleticBoard();
}
