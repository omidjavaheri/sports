package ir.sports.entities.context.board;

import javax.persistence.*;

@Entity
@Table(name = "staff")
public class Staff {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String information;
    @ManyToOne
    private AthleticBoard athleticBoard;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public AthleticBoard getAthleticBoard() {
        return athleticBoard;
    }

    public void setAthleticBoard(AthleticBoard athleticBoard) {
        this.athleticBoard = athleticBoard;
    }
}
