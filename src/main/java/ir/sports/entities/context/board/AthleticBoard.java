package ir.sports.entities.context.board;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "athletic_board")
public class AthleticBoard {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String province;
    private String city;
    private String galleryJson;
    private String advanceEducationalContentJson;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGalleryJson() {
        return galleryJson;
    }

    public void setGalleryJson(String galleryJson) {
        this.galleryJson = galleryJson;
    }

    public String getAdvanceEducationalContentJson() {
        return advanceEducationalContentJson;
    }

    public void setAdvanceEducationalContentJson(String advanceEducationalContentJson) {
        this.advanceEducationalContentJson = advanceEducationalContentJson;
    }

}
