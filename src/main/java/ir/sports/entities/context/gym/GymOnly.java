package ir.sports.entities.context.gym;

public interface GymOnly {
    Gym getGym();
}
