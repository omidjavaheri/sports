package ir.sports.entities.context.gym;

import ir.sports.entities.context.Sport;

import javax.persistence.*;

@Entity
@Table(name = "gym_sport")
public class GymSport {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private Gym gym;
    @ManyToOne
    private Sport sport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Gym getGym() {
        return gym;
    }

    public void setGym(Gym gym) {
        this.gym = gym;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }
}
