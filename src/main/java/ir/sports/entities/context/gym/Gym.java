package ir.sports.entities.context.gym;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "gym")
public class Gym {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String province;
    private String city;
    private String contactInfoJson;
    private String galleryJson;
    private String titlesAndHonors;
    private Boolean approved;
    @Transient
    private List<Long> sportsId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getContactInfoJson() {
        return contactInfoJson;
    }

    public void setContactInfoJson(String contactInfoJson) {
        this.contactInfoJson = contactInfoJson;
    }

    public String getGalleryJson() {
        return galleryJson;
    }

    public void setGalleryJson(String galleryJson) {
        this.galleryJson = galleryJson;
    }

    public String getTitlesAndHonors() {
        return titlesAndHonors;
    }

    public void setTitlesAndHonors(String titlesAndHonors) {
        this.titlesAndHonors = titlesAndHonors;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public List<Long> getSportsId() {
        return sportsId;
    }

    public void setSportsId(List<Long> sportsId) {
        this.sportsId = sportsId;
    }
}
