package ir.sports.entities.context.mentor;

import ir.sports.entities.context.Sport;

import javax.persistence.*;

@Entity
@Table(name = "mentor_sport")
public class MentorSport {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private Mentor mentor;
    @ManyToOne
    private Sport sport;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Mentor getMentor() {
        return mentor;
    }

    public void setMentor(Mentor mentor) {
        this.mentor = mentor;
    }

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }
}
