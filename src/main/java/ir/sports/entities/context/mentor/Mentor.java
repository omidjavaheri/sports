package ir.sports.entities.context.mentor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ir.sports.entities.User;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "mentor")
public class Mentor {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @JsonIgnore
    private String username;
    private String province;
    private String city;
    private String personalInformation;
    private String currentClasses;
    private String professionalRecord;
    private String coachingAndProfessionalQualifications;
    private String galleryJson;
    private String titlesAndHonors;
    private Boolean active;
    private Boolean approved;
    @Transient
    private List<Long> sportsId;
    @Transient
    private User user;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPersonalInformation() {
        return personalInformation;
    }

    public void setPersonalInformation(String personalInformation) {
        this.personalInformation = personalInformation;
    }

    public String getCurrentClasses() {
        return currentClasses;
    }

    public void setCurrentClasses(String currentClasses) {
        this.currentClasses = currentClasses;
    }

    public String getProfessionalRecord() {
        return professionalRecord;
    }

    public void setProfessionalRecord(String professionalRecord) {
        this.professionalRecord = professionalRecord;
    }

    public String getCoachingAndProfessionalQualifications() {
        return coachingAndProfessionalQualifications;
    }

    public void setCoachingAndProfessionalQualifications(String coachingAndProfessionalQualifications) {
        this.coachingAndProfessionalQualifications = coachingAndProfessionalQualifications;
    }

    public String getGalleryJson() {
        return galleryJson;
    }

    public void setGalleryJson(String galleryJson) {
        this.galleryJson = galleryJson;
    }

    public String getTitlesAndHonors() {
        return titlesAndHonors;
    }

    public void setTitlesAndHonors(String titlesAndHonors) {
        this.titlesAndHonors = titlesAndHonors;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public List<Long> getSportsId() {
        return sportsId;
    }

    public void setSportsId(List<Long> sportsId) {
        this.sportsId = sportsId;
    }
}
