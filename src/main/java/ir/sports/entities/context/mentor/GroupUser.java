package ir.sports.entities.context.mentor;

import javax.persistence.*;

@Entity
@Table(name = "group_user")
public class GroupUser {
    @Id
    @GeneratedValue
    private Long id;
    private String username;
    @ManyToOne
    private Group group;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
