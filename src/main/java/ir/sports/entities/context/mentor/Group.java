package ir.sports.entities.context.mentor;

import javax.persistence.*;

@Entity
@Table(name = "event_group")
public class Group {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @ManyToOne
    private Mentor mentor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Mentor getMentor() {
        return mentor;
    }

    public void setMentor(Mentor mentor) {
        this.mentor = mentor;
    }
}
