package ir.sports.entities.context;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "sport")
public class Sport {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String introduction;
    private String history;
    private String educationalContentJson;
    private String galleryJson;
    @Transient
    private List<Long> athleticBoardsId;

    public List<Long> getAthleticBoardsId() {
        return athleticBoardsId;
    }

    public void setAthleticBoardsId(List<Long> athleticBoardsId) {
        this.athleticBoardsId = athleticBoardsId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getEducationalContentJson() {
        return educationalContentJson;
    }

    public void setEducationalContentJson(String educationalContentJson) {
        this.educationalContentJson = educationalContentJson;
    }

    public String getGalleryJson() {
        return galleryJson;
    }

    public void setGalleryJson(String galleryJson) {
        this.galleryJson = galleryJson;
    }
}
