package ir.sports.entities.context;

public interface SportOnly {
    Sport getSport();
}
