package ir.sports.entities.app;

public interface IdAndUsernameAndProfilePictureOnly {
    Long getId();
    String getUsername();
    String getProfilePicture();
}
