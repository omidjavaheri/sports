package ir.sports.entities.app;

import ir.sports.entities.User;

import javax.persistence.*;

@Entity
@Table(name = "relation", uniqueConstraints = @UniqueConstraint(columnNames = {"follower_id", "followee_id"}))
public class Relation {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private User follower;
    @ManyToOne
    private User followee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getFollower() {
        return follower;
    }

    public void setFollower(User follower) {
        this.follower = follower;
    }

    public User getFollowee() {
        return followee;
    }

    public void setFollowee(User followee) {
        this.followee = followee;
    }
}
