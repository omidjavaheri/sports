package ir.sports.entities.app;

public interface FollowerOnly {
    IdAndUsernameAndProfilePictureOnly getFollower();
}
