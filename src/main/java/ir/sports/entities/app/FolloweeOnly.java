package ir.sports.entities.app;

public interface FolloweeOnly {
    IdAndUsernameAndProfilePictureOnly getFollowee();
}
