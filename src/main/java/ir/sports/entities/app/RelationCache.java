package ir.sports.entities.app;

import javax.persistence.*;

@Entity
@Table(name = "relation_cache")
public class RelationCache {
    @Id
    @GeneratedValue
    private Long id;
    @Column(name = "username", unique = true)
    private String username;
    private Long userId;
    private String followersList;
    private String followingsList;
    private boolean followersUpdated;
    private boolean followingsUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFollowersList() {
        return followersList;
    }

    public void setFollowersList(String followersList) {
        this.followersList = followersList;
    }

    public String getFollowingsList() {
        return followingsList;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setFollowingsList(String followingsList) {
        this.followingsList = followingsList;
    }

    public boolean isFollowersUpdated() {
        return followersUpdated;
    }

    public void setFollowersUpdated(boolean followersUpdated) {
        this.followersUpdated = followersUpdated;
    }

    public boolean isFollowingsUpdated() {
        return followingsUpdated;
    }

    public void setFollowingsUpdated(boolean followingsUpdated) {
        this.followingsUpdated = followingsUpdated;
    }
}
