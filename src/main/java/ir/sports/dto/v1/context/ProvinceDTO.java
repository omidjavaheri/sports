package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;

public class ProvinceDTO {
    private String province;
    private PageableDTO pageableDTO;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
