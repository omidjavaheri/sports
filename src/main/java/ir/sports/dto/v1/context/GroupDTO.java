package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.mentor.Group;

public class GroupDTO {
    private Group group;
    private PageableDTO pageableDTO;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
