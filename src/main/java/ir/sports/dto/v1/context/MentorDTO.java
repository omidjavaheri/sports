package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.mentor.Mentor;

public class MentorDTO {
    private Mentor mentor;
    private PageableDTO pageableDTO;

    public Mentor getMentor() {
        return mentor;
    }

    public void setMentor(Mentor mentor) {
        this.mentor = mentor;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
