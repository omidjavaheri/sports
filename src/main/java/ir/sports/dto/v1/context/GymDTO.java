package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.gym.Gym;

public class GymDTO {
    private Gym gym;
    private PageableDTO pageableDTO;

    public Gym getGym() {
        return gym;
    }

    public void setGym(Gym gym) {
        this.gym = gym;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
