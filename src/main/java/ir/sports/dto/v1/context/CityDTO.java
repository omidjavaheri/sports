package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;

public class CityDTO {
    private String city;
    private PageableDTO pageableDTO;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
