package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.board.AthleticBoard;

public class AthleticBoardDTO {
    private AthleticBoard athleticBoard;
    private PageableDTO pageableDTO;

    public AthleticBoard getAthleticBoard() {
        return athleticBoard;
    }

    public void setAthleticBoard(AthleticBoard athleticBoard) {
        this.athleticBoard = athleticBoard;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
