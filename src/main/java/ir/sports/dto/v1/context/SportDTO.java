package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.Sport;

public class SportDTO {
    private Sport sport;
    private PageableDTO pageableDTO;

    public Sport getSport() {
        return sport;
    }

    public void setSport(Sport sport) {
        this.sport = sport;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
