package ir.sports.dto.v1.context;

import ir.sports.dto.v1.PageableDTO;

public class NameDTO {
    private String name;
    private PageableDTO pageableDTO;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PageableDTO getPageableDTO() {
        return pageableDTO;
    }

    public void setPageableDTO(PageableDTO pageableDTO) {
        this.pageableDTO = pageableDTO;
    }
}
