package ir.sports.controllers.app.socialMedia;

import com.fasterxml.jackson.core.JsonProcessingException;
import ir.sports.entities.User;
import ir.sports.entities.app.FollowerOnly;
import ir.sports.services.app.RelationService;
import ir.sports.services.app.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/app/user")
public class FollowController {
    @Autowired
    private RelationService relationService;
    @Autowired
    private RequestService requestService;

    @PostMapping("/follow/findFollowers")
    @ResponseBody
    public String findAllFollowers() throws JsonProcessingException {
        return relationService.findAllFollowers();
    }

    @PostMapping("/follow/findFollowings")
    @ResponseBody
    public String findAllFollowings() throws JsonProcessingException {
        return relationService.findAllFollowings();
    }

    @PostMapping("/follow/unfollow")
    @ResponseBody
    public void unfollow(@RequestBody User follow) {
        relationService.unfollow(follow);
    }

    @PostMapping("/follow/remove")
    @ResponseBody
    public void removeFollower(@RequestBody User follower) {
        relationService.removeFollower(follower);
    }

    @PostMapping("/request/save")
    @ResponseBody
    public void sendRequest(@RequestBody User follow) {
        requestService.sendRequest(follow);
    }

    @PostMapping("/request/find")
    @ResponseBody
    public List<FollowerOnly> findAllReceivedRequest() {
        return requestService.findAllReceivedRequest();
    }

    @PostMapping("/request/accept")
    @ResponseBody
    public void acceptRequest(@RequestBody User follower) {
        requestService.acceptRequest(follower);
    }

    @PostMapping("/request/reject")
    @ResponseBody
    public void rejectRequest(@RequestBody User follower) {
        requestService.rejectRequest(follower);
    }

    @PostMapping("/request/delete")
    @ResponseBody
    public void unsentRequest(@RequestBody User follow) {
        requestService.unsentRequest(follow);
    }
}
