package ir.sports.controllers.app;

import ir.sports.dto.v1.context.SportDTO;
import ir.sports.entities.context.mentor.MentorOnly;
import ir.sports.services.context.mentor.MentorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/app/mentor")
public class MentorController {
    @Autowired
    private MentorService mentorService;

    @PostMapping("/findBySport")
    @ResponseBody
    public List<MentorOnly> findBySport(@RequestBody SportDTO dto) {
        return mentorService.findAllBySport(dto);
    }
}
