package ir.sports.controllers.app;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.GroupDTO;
import ir.sports.entities.context.Event;
import ir.sports.entities.context.mentor.Group;
import ir.sports.entities.context.mentor.GroupOnly;
import ir.sports.services.context.mentor.GroupSubscribeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/group")
public class GroupSubscribeController {
    @Autowired
    private GroupSubscribeService groupSubscribeService;

    @PostMapping("/find")
    @ResponseBody
    public Iterable<GroupOnly> find(@RequestBody PageableDTO dto) {
        return groupSubscribeService.findAll(dto);
    }

    @PostMapping("/follow")
    @ResponseBody
    public void follow(@RequestBody Group dto) {
        groupSubscribeService.follow(dto);
    }

    @PostMapping("/unfollow")
    @ResponseBody
    public void unfollow(@RequestBody Group group) {
        groupSubscribeService.unfollow(group);
    }

    @PostMapping("/findByGroup")
    @ResponseBody
    public Iterable<Event> findByGroup(@RequestBody GroupDTO dto) {
        return groupSubscribeService.findAllGroupEvents(dto);
    }
}
