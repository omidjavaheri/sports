package ir.sports.controllers.app;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.NameDTO;
import ir.sports.entities.context.Sport;
import ir.sports.services.context.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/app/sport")
public class SportController {
    @Autowired
    private SportService sportService;

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Sport> find(@RequestBody PageableDTO dto) {
        return sportService.findAll(dto);
    }

    @PostMapping("/search")
    @ResponseBody
    public Iterable<Sport> search(@RequestBody NameDTO dto) {
        return sportService.findAllByNameContaining(dto);
    }

    @PostMapping("/findById")
    @ResponseBody
    public Sport findById(@RequestBody Long id) {
        return sportService.findById(id);
    }
}
