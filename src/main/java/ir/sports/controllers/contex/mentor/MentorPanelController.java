package ir.sports.controllers.contex.mentor;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.mentor.Course;
import ir.sports.entities.context.mentor.Group;
import ir.sports.services.context.mentor.CourseService;
import ir.sports.services.context.mentor.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/mentor")
public class MentorPanelController {
    @Autowired
    private CourseService courseService;
    @Autowired
    private GroupService groupService;

    @PostMapping("/course/save")
    @ResponseBody
    public void registerCourse(@RequestBody Course dto) {
        courseService.save(dto);
    }

    @PostMapping("/course/delete")
    @ResponseBody
    public void deleteCourse(@RequestBody Long id) {
        courseService.deleteById(id);
    }

    @PostMapping("/course/find")
    @ResponseBody
    public Iterable<Course> findCourse(@RequestBody PageableDTO dto) {
        return courseService.findAll(dto);
    }

    @PostMapping("/group/save")
    @ResponseBody
    public void registerGroup(@RequestBody Group dto) {
        groupService.save(dto);
    }

    @PostMapping("/group/delete")
    @ResponseBody
    public void deleteGroup(@RequestBody Long id) {
        groupService.deleteById(id);
    }

    @PostMapping("/group/find")
    @ResponseBody
    public Iterable<Group> findGroup(@RequestBody PageableDTO dto) {
        return groupService.findAll(dto);
    }
}
