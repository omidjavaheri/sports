package ir.sports.controllers.contex.admin;

import ir.sports.dto.v1.context.*;
import ir.sports.entities.context.SportOnly;
import ir.sports.entities.context.gym.Gym;
import ir.sports.entities.context.gym.GymOnly;
import ir.sports.services.context.gym.GymService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/admin/gym")
public class AdminGymController {
    @Autowired
    private GymService gymService;

    @PostMapping("/save")
    @ResponseBody
    public void register(@RequestBody Gym dto) {
        gymService.save(dto);
    }

    @PostMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        gymService.deleteById(id);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Gym> find() {
        return gymService.findAll();
    }

    @PostMapping("/findByName")
    @ResponseBody
    public List<Gym> findByName(@RequestBody NameDTO dto) {
        return gymService.findAllByName(dto);
    }

    @PostMapping("/findByProvince")
    @ResponseBody
    public List<Gym> findByProvince(@RequestBody ProvinceDTO dto) {
        return gymService.findAllByProvince(dto);
    }

    @PostMapping("/findByCity")
    @ResponseBody
    public List<Gym> findByCity(@RequestBody CityDTO dto) {
        return gymService.findAllByCity(dto);
    }

    @PostMapping("/findBySport")
    @ResponseBody
    public List<GymOnly> findBySport(@RequestBody SportDTO dto) {
        return gymService.findAllGymBySport(dto);
    }

    @PostMapping("/findAllSports")
    @ResponseBody
    public List<SportOnly> findAllSports(@RequestBody GymDTO dto) {
        return gymService.findAllSports(dto);
    }

    @PostMapping("/findOne")
    @ResponseBody
    public Gym findById(@RequestBody Long id) {
        return gymService.findById(id);
    }
}
