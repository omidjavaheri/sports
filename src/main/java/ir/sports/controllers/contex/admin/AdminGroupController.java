package ir.sports.controllers.contex.admin;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.MentorDTO;
import ir.sports.entities.context.mentor.Group;
import ir.sports.services.context.mentor.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/admin/group")
public class AdminGroupController {
    @Autowired
    private GroupService groupService;

    @PostMapping("/save")
    @ResponseBody
    public void register(@RequestBody Group dto) {
        groupService.adminSave(dto);
    }

    @PostMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        groupService.adminDeleteById(id);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Group> find(@RequestBody PageableDTO dto) {
        return groupService.adminFindAll(dto);
    }

    @PostMapping("/findByMentor")
    @ResponseBody
    public Iterable<Group> findAllById(@RequestBody MentorDTO dto) {
        return groupService.findAllByMentor(dto);
    }
}
