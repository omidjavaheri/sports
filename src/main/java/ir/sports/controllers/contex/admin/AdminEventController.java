package ir.sports.controllers.contex.admin;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.Event;
import ir.sports.services.context.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/admin/event")
public class AdminEventController {
    @Autowired
    private EventService eventService;

    @PostMapping("/save")
    @ResponseBody
    public void register(@RequestBody Event dto) {
        eventService.save(dto);
    }

    @PostMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        eventService.deleteById(id);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Event> find(@RequestBody PageableDTO dto) {
        return eventService.findAll(dto);
    }

    @PostMapping("/findById")
    @ResponseBody
    public Event findById(@RequestBody Long id){
        return eventService.findById(id);
    }
}
