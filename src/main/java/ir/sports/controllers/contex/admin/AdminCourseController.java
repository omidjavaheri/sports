package ir.sports.controllers.contex.admin;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.dto.v1.context.MentorDTO;
import ir.sports.entities.context.mentor.Course;
import ir.sports.services.context.mentor.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/admin/course")
public class AdminCourseController {
    @Autowired
    private CourseService courseService;

    @PostMapping("/save")
    @ResponseBody
    public void register(@RequestBody Course dto) {
        courseService.adminSave(dto);
    }

    @PostMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        courseService.adminDeleteById(id);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Course> find(@RequestBody PageableDTO dto) {
        return courseService.adminFindAll(dto);
    }

    @PostMapping("/findByMentor")
    @ResponseBody
    public Iterable<Course> findAllById(@RequestBody MentorDTO dto) {
        return courseService.findAllByMentor(dto);
    }
}
