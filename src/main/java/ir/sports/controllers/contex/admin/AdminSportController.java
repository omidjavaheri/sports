package ir.sports.controllers.contex.admin;

import ir.sports.dto.v1.PageableDTO;
import ir.sports.entities.context.Sport;
import ir.sports.services.context.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/admin/sport")
public class AdminSportController {
    @Autowired
    private SportService sportService;

    @PostMapping("/save")
    @ResponseBody
    public void register(@RequestBody Sport dto) {
        sportService.save(dto);
    }

    @PostMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        sportService.deleteById(id);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Sport> find(@RequestBody PageableDTO dto) {
        return sportService.findAll(dto);
    }

    @PostMapping("/findById")
    @ResponseBody
    public Sport findById(@RequestBody Long id) {
        return sportService.findById(id);
    }
}
