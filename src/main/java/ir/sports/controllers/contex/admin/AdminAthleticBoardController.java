package ir.sports.controllers.contex.admin;

import ir.sports.dto.v1.context.*;
import ir.sports.entities.context.SportOnly;
import ir.sports.entities.context.board.AthleticBoard;
import ir.sports.entities.context.board.AthleticBoardOnly;
import ir.sports.entities.context.board.News;
import ir.sports.entities.context.board.Staff;
import ir.sports.services.context.board.AthleticBoardService;
import ir.sports.services.context.board.NewsService;
import ir.sports.services.context.board.StaffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/admin/board")
public class AdminAthleticBoardController {
    @Autowired
    private AthleticBoardService athleticBoardService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private StaffService staffService;

    @PostMapping("/save")
    @ResponseBody
    public void register(@RequestBody AthleticBoard dto) {
        athleticBoardService.save(dto);
    }

    @PostMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        athleticBoardService.deleteById(id);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<AthleticBoard> find() {
        return athleticBoardService.findAll();
    }

    @PostMapping("/findByName")
    @ResponseBody
    public List<AthleticBoard> findByName(@RequestBody NameDTO dto) {
        return athleticBoardService.findAllByName(dto);
    }

    @PostMapping("/findByProvince")
    @ResponseBody
    public List<AthleticBoard> findByProvince(@RequestBody ProvinceDTO dto) {
        return athleticBoardService.findAllByProvince(dto);
    }

    @PostMapping("/findByCity")
    @ResponseBody
    public List<AthleticBoard> findByCity(@RequestBody CityDTO dto) {
        return athleticBoardService.findAllByCity(dto);
    }

    @PostMapping("/findBySport")
    @ResponseBody
    public List<AthleticBoardOnly> findBySport(@RequestBody SportDTO dto) {
        return athleticBoardService.findAllBySport(dto);
    }

    @PostMapping("/findAllSports")
    @ResponseBody
    public List<SportOnly> findAllSports(@RequestBody AthleticBoardDTO dto) {
        return athleticBoardService.findAllSports(dto);
    }

    @PostMapping("/news/find")
    @ResponseBody
    public List<News> findAllNews(@RequestBody AthleticBoardDTO dto) {
        return newsService.findAllByAthleticBoard(dto);
    }

    @PostMapping("/news/save")
    @ResponseBody
    public void registerNews(@RequestBody News dto) {
        newsService.save(dto);
    }

    @PostMapping("/news/delete")
    @ResponseBody
    public void deleteNews(@RequestBody Long id) {
        newsService.deleteById(id);
    }

    @PostMapping("/staff/find")
    @ResponseBody
    public List<Staff> findAllStaff(@RequestBody AthleticBoardDTO dto) {
        return staffService.findAllByAthleticBoard(dto);
    }

    @PostMapping("/staff/save")
    @ResponseBody
    public void registerStaff(@RequestBody Staff dto) {
        staffService.save(dto);
    }

    @PostMapping("/staff/delete")
    @ResponseBody
    public void deleteStaff(@RequestBody Long id) {
        staffService.deleteById(id);
    }

    @PostMapping("/findOne")
    @ResponseBody
    public AthleticBoard findById(@RequestBody Long id) {
        return athleticBoardService.findById(id);
    }
}
