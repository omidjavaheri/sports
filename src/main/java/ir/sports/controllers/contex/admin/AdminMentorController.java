package ir.sports.controllers.contex.admin;

import ir.sports.dto.v1.context.*;
import ir.sports.entities.context.SportOnly;
import ir.sports.entities.context.mentor.Mentor;
import ir.sports.entities.context.mentor.MentorOnly;
import ir.sports.services.context.mentor.MentorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/admin/mentor")
public class AdminMentorController {
    @Autowired
    private MentorService mentorService;

    @PostMapping("/save")
    @ResponseBody
    public void register(@RequestBody Mentor dto) {
        mentorService.save(dto);
    }

    @PostMapping("/delete")
    @ResponseBody
    public void delete(@RequestBody Long id) {
        mentorService.deleteById(id);
    }

    @PostMapping("/find")
    @ResponseBody
    public Iterable<Mentor> find() {
        return mentorService.findAll();
    }

    @PostMapping("/findByName")
    @ResponseBody
    public List<Mentor> findByName(@RequestBody NameDTO dto) {
        return mentorService.findAllByName(dto);
    }

    @PostMapping("/findByProvince")
    @ResponseBody
    public List<Mentor> findByProvince(@RequestBody ProvinceDTO dto) {
        return mentorService.findAllByProvince(dto);
    }

    @PostMapping("/findByCity")
    @ResponseBody
    public List<Mentor> findByCity(@RequestBody CityDTO dto) {
        return mentorService.findAllByCity(dto);
    }

    @PostMapping("/findBySport")
    @ResponseBody
    public List<MentorOnly> findBySport(@RequestBody SportDTO dto) {
        return mentorService.findAllBySport(dto);
    }
    @PostMapping("/findAllSports")
    @ResponseBody
    public List<SportOnly> findAllSports(@RequestBody MentorDTO dto) {
        return mentorService.findAllSports(dto);
    }

    @PostMapping("/findById")
    @ResponseBody
    public Mentor findById(@RequestBody Long id) {
        return mentorService.findById(id);
    }
}
