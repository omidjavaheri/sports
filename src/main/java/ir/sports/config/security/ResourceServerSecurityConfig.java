package ir.sports.config.security;

import ir.sports.entities.Role;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerSecurityConfig extends ResourceServerConfigurerAdapter {

    private static final String RESOURCE_ID = "resource_id";

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(RESOURCE_ID).stateless(false);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http/*.
                 anonymous().disable()*/
                .authorizeRequests()
                .antMatchers("/actuator/**").hasAuthority(Role.ACTUXXATOR_ADMXXIN.getServer())
//                .antMatchers("/**/admin/**").hasAuthority(Role.ADXXMIN.getServer())
                .antMatchers("/**/admin/**").anonymous()
                .antMatchers("/**/v1/mentor/**").hasAuthority(Role.MENXXTOR.getServer())
                .antMatchers("/**/v1/group/**").hasAuthority(Role.USXXER.getServer())
                .antMatchers("/**/app/**").hasAuthority(Role.USXXER.getServer())
                .antMatchers("/**/user/authenticated/**").permitAll()
                .antMatchers("/**/user/anonymous/**").anonymous()
                .anyRequest().denyAll()
                .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }
}
